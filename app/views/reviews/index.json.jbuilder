json.array!(@reviews) do |review|
  json.extract! review, :id, :name, :email, :user_id, :hotel_id, :phone, :tour, :review
  json.url review_url(review, format: :json)
end
