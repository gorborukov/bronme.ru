json.array!(@photos) do |photo|
  json.extract! photo, :id, :name, :hotel_id
  json.url photo_url(photo, format: :json)
end
