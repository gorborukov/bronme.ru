json.array!(@hotels) do |hotel|
  json.extract! hotel, :id, :name, :description, :short_description, :additional, :checkin, :checkout, :minimal_price, :price_url, :address, :phone, :resort_id
  json.url hotel_url(hotel, format: :json)
end
