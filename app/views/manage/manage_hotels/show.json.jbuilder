json.extract! @hotel, :id, :name, :description, :short_description, :additional, :checkin, :checkout, :minimal_price, :price_url, :address, :phone, :resort_id, :created_at, :updated_at
