json.array!(@resorts) do |resort|
  json.extract! resort, :id, :name, :description, :short_description, :popular
  json.url resort_url(resort, format: :json)
end
