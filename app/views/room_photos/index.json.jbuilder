json.array!(@room_photos) do |room_photo|
  json.extract! room_photo, :id, :name, :room_id
  json.url room_photo_url(room_photo, format: :json)
end
