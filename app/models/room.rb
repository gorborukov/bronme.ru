class Room < ActiveRecord::Base
	belongs_to :room
	has_many :room_photos
	accepts_nested_attributes_for :room_photos, :allow_destroy => true
end
