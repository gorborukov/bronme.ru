class Service < ActiveRecord::Base
	has_many :hotel_services
	has_many :hotels, through: :hotel_services
end
