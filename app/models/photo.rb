class Photo < ActiveRecord::Base
	belongs_to :hotel

	has_attached_file :image, :styles => { :original => '900x600#', :hotel => "155x96#", :hotel_gallery => "140x105#", :room_big => "300x200#", :room_square => "200x200#", :room_thumb => "96x96#" }
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
