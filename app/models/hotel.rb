class Hotel < ActiveRecord::Base
	belongs_to :resort
	has_many :hotel_services
	has_many :services, through: :hotel_services
	has_many :photos
	has_many :rooms
	has_many :reviews
	accepts_nested_attributes_for :photos, :allow_destroy => true

	has_attached_file :image, :styles => { :original => "900x600>", :square => "200x200#", :rectangle => "285x146#", :slider => "240x160#", :thumbnail => "70x70#" }
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
