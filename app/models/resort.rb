class Resort < ActiveRecord::Base
	has_many :hotels

	has_attached_file :image, :styles => { :original => "900x600>", :large => "1280x400#", :square => "150x150#" }
    validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
end
