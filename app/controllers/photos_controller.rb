class PhotosController < ApplicationController
  before_action :set_photo, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @photos = Photo.all
    respond_with(@photos)
  end

  def show
    respond_with(@photo)
  end

  def new
    @photo = Photo.new
    respond_with(@photo)
  end

  def edit
  end

  def create
    photo = Photo.create(:image => params[:file], :hotel_id => params[:hotel_id])
    render :json => {:success => true}
  end

  def update
    @photo.update(photo_params)
    redirect_to manage_manage_hotels_path
  end

  def destroy
    @photo.destroy
    redirect_to :back
  end

  private
    def set_photo
      @photo = Photo.find(params[:id])
    end

    def photo_params
      params.require(:photo).permit(:name, :hotel_id, :room_id, :image, :image_file_name)
    end
end
