class HotelsController < ApplicationController
  before_action :set_hotel, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @hotels = Hotel.all
    respond_with(@hotels)
  end

  def show
    @reviews = @hotel.reviews
    @review = Review.new
    session[:hotel_id] = @hotel.id
    respond_with(@hotel)
  end

  def new
    @hotel = Hotel.new
    respond_with(@hotel)
  end

  def edit
  end

  def create
    @hotel = Hotel.new(hotel_params)
    @hotel.save
    redirect_to manage_manage_hotels_path
  end

  def update
    @hotel.update(hotel_params)
    redirect_to manage_manage_hotels_path
  end

  def destroy
    @hotel.destroy
    redirect_to manage_manage_hotels_path
  end

  private
    def set_hotel
      @hotel = Hotel.find(params[:id])
    end

    def hotel_params
      params.require(:hotel).permit(:name, :description, :short_description, :additional, :checkin, :checkout, :minimal_price, :price_url, :address, :phone, :resort_id, :image, :latitude, :longitude, :special, :banner, service_ids: [])
    end
end
