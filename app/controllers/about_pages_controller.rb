class AboutPagesController < ApplicationController
  before_action :set_about_page, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @about_pages = AboutPage.all
    respond_with(@about_pages)
  end

  def show
    respond_with(@about_page)
  end

  def new
    @about_page = AboutPage.new
    respond_with(@about_page)
  end

  def edit
  end

  def create
    @about_page = AboutPage.new(about_page_params)
    @about_page.save
    redirect_to manage_manage_about_pages_path
  end

  def update
    @about_page.update(about_page_params)
    redirect_to manage_manage_about_pages_path
  end

  def destroy
    @about_page.destroy
    redirect_to manage_manage_about_pages_path
  end

  private
    def set_about_page
      @about_page = AboutPage.find(params[:id])
    end

    def about_page_params
      params.require(:about_page).permit(:name, :slug, :content)
    end
end
