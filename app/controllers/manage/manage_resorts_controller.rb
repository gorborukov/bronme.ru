class Manage::ManageResortsController < ApplicationController
  before_action :set_resort, only: [:show, :edit, :update, :destroy]
  layout 'manage'
  respond_to :html

  def index
    @resorts = Resort.all
    respond_with(@resorts)
  end

  def show
    respond_with(@resort)
  end

  def new
    @resort = Resort.new
    respond_with(@resort)
  end

  def edit
  end

  def create
    @resort = Resort.new(resort_params)
    @resort.save
    respond_with(@resort)
  end

  def update
    @resort.update(resort_params)
    respond_with(@resort)
  end

  def destroy
    @resort.destroy
    respond_with(@resort)
  end

  private
    def set_resort
      @resort = Resort.find(params[:id])
    end

    def resort_params
      params.require(:resort).permit(:name, :description, :short_description, :popular, :image)
    end
end
