class Manage::ManageUsersController < ApplicationController
  around_filter :catch_not_found
  prepend_before_filter :require_no_authentication, only: [:cancel ]
  layout 'manage'

  def show
    @user = User.find(params[:id])
  end

  def index
    @user = User.new
    @users = User.all
  end

  def edit
    if current_user || current_user.role = 'superuser'
  	  @user = current_user
    else
      redirect_to not_found_path
    end
  end
  
  def update
    @user = current_user 
    if user_params[:password].blank?
      user_params.delete :password
      user_params.delete :password_confirmation
    end
    
    if @user.update(user_params)
      sign_in @user, :bypass => true
      redirect_to @user, notice: 'User was successfully updated.'
    else
      render action: 'edit'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :phone, :skype, :payment, :job, :email, :password, :current_password, :role_id)
    end

    def needs_password?(user, params)
      params[:password].present?
    end

    def after_update_path_for(resource)
      user_path(current_user)
    end

    def catch_not_found
      yield
    rescue ActiveRecord::RecordNotFound
      redirect_to not_found_path
    end
end