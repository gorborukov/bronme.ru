class Manage::ManageHotelsController < ApplicationController
  before_action :set_hotel, only: [:show, :edit, :update, :destroy]
  layout 'manage'
  respond_to :html

  def index
    @hotels = Hotel.all
    respond_with(@hotels)
  end

  def rooms
    @hotel = Hotel.find(params[:id])
    @rooms = @hotel.rooms
    @room = Room.new
    session[:hotel_id] = @hotel.id
  end

  def show
    respond_with(@hotel)
  end

  def new
    @hotel = Hotel.new
    respond_with(@hotel)
  end

  def edit
  end

  def create
    @hotel = Hotel.new(hotel_params)
    @hotel.save
    respond_with(@hotel)
  end

  def update
    @hotel.update(hotel_params)
    respond_with(@hotel)
  end

  def destroy
    @hotel.destroy
    respond_with(@hotel)
  end

  def upload
    @hotel = Hotel.find(params[:id])
    @photos = @hotel.photos
  end

  private
    def set_hotel
      @hotel = Hotel.find(params[:id])
    end

    def hotel_params
      params.require(:hotel).permit(:name, :description, :short_description, :additional, :checkin, :checkout, :minimal_price, :price_url, :address, :phone, :resort_id, :image, :latitude, :longitude, :special, :banner, service_ids: [])
    end
end