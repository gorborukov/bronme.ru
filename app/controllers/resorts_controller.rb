class ResortsController < ApplicationController
  before_action :set_resort, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @resorts = Resort.all
    respond_with(@resorts)
  end

  def show
    respond_with(@resort)
  end

  def new
    @resort = Resort.new
    respond_with(@resort)
  end

  def edit
  end

  def create
    @resort = Resort.new(resort_params)
    @resort.save
    redirect_to manage_manage_resorts_path
  end

  def update
    @resort.update(resort_params)
    redirect_to manage_manage_resorts_path
  end

  def destroy
    @resort.destroy
    redirect_to manage_manage_resorts_path
  end

  private
    def set_resort
      @resort = Resort.find(params[:id])
    end

    def resort_params
      params.require(:resort).permit(:name, :description, :short_description, :popular, :image)
    end
end
