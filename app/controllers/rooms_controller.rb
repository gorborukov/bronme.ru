class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @rooms = Room.all
    respond_with(@rooms)
  end

  def show
    respond_with(@room)
  end

  def new
    @room = Room.new
    respond_with(@room)
  end

  def edit
  end

  def create
    @room = Room.new(room_params)
    @room.save
    redirect_to :back
  end

  def update
    @room.update(room_params)
    redirect_to manage_manage_hotels_rooms_path(session[:hotel_id])
  end

  def destroy
    @room.destroy
    redirect_to manage_manage_hotels_rooms_path(session[:hotel_id])
  end

  private
    def set_room
      @room = Room.find(params[:id])
    end

    def room_params
      params.require(:room).permit(:name, :description, :hotel_id, room_photos_attributes: [:image])
    end
end
