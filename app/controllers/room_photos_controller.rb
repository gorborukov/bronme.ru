class RoomPhotosController < ApplicationController
  before_action :set_room_photo, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @room_photos = RoomPhoto.all
    respond_with(@room_photos)
  end

  def show
    respond_with(@room_photo)
  end

  def new
    @room_photo = RoomPhoto.new
    respond_with(@room_photo)
  end

  def edit
  end

  def create
    @room_photo = RoomPhoto.new(room_photo_params)
    @room_photo.save
    redirect_to :back
  end

  def update
    @room_photo.update(room_photo_params)
    respond_with(@room_photo)
  end

  def destroy
    @room_photo.destroy
    redirect_to :back
  end

  private
    def set_room_photo
      @room_photo = RoomPhoto.find(params[:id])
    end

    def room_photo_params
      params.require(:room_photo).permit(:name, :room_id, :image, :image_file_name)
    end
end
