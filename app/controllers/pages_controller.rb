class PagesController < ApplicationController
	respond_to :html, :xml, :json
    before_action :set_page, only: [:show, :edit, :update, :destroy]
	
	def show
		respond_with(@page)
	end

	def index
		@resorts = Resort.all
	end

	def payment
		@resorts = Resort.all
	end

	def create
		@page = Page.new(page_params)
		@page.save
		redirect_to manage_manage_pages_path
	end

	def update
		@page.update(page_params)
		redirect_to manage_manage_pages_path
	end

	def destroy
		@page.destroy
		redirect_to manage_manage_pages_path
	end

	private
	    def set_page
	      @page = Page.find(params[:id])
	    end

	    def page_params
	      params.require(:page).permit(:name, :slug, :content)
	    end
end
