class UsersController < ApplicationController
  around_filter :catch_not_found
  prepend_before_filter :require_no_authentication, only: [:cancel ]
  layout :choose_layout

  def show
    @user = User.find(params[:id])
    @message = Message.new
    messages = Message.arel_table
    @messages = Message.all.where(messages[:reciever_id].eq(@user.id).or(messages[:sender_id].eq(@user.id))).order('created_at DESC')
    @event = Event.new
    @events = Event.where(:user_id => @user.id).order('created_at DESC')
  end

  def profile
    @user = current_user
    @events = Event.where(:user_id => @user.id).order('created_at DESC')
    @message = Message.new
    @manager = User.all.where(:id => @user.manager_id).first
    messages = Message.arel_table
    @messages = Message.all.where(messages[:reciever_id].eq(@user.id).or(messages[:sender_id].eq(@user.id))).order('created_at DESC')
  end

  def index
    @users = User.all
  end

  def edit
    if current_user
      if current_user.role_id == 1
  	    @user = current_user
      elsif current_user.role_id == 6
        @user = User.find(params[:id])
      end
    else
      redirect_to not_found_path
    end
  end

  def create
    @user = User.new(user_params)
    @user.save
    redirect_to manage_manage_users_path
  end

  def update
    if current_user.role_id == 1
      @user = current_user
    elsif current_user.role_id == 4
      @user = current_user
    elsif current_user.role_id == 6
      @user = User.find(params[:id])
    end 
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
    end
    
    if @user.update(user_params) && current_user.role_id != 6
      redirect_to user_profile_path, notice: 'User was successfully updated.'
    elsif @user.update(user_params) && current_user.role_id == 6
      redirect_to manage_manage_users_path
    else
      render action: 'edit'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:username, :phone, :skype, :payment, :job, :email, :password, :current_password, :role_id, :manager_id)
    end

    def needs_password?(user, params)
      params[:password].present?
    end

    def after_update_path_for(resource)
      user_path(current_user)
    end

    def catch_not_found
      yield
    rescue ActiveRecord::RecordNotFound
      redirect_to not_found_path
    end

    def choose_layout
      case action_name
      when 'edit'
        'manage'
      else
        'application'
      end
    end
end