var token = $( 'meta[name="csrf-token"]' ).attr( 'content' );
var uploader = new plupload.Uploader({
runtimes : 'html5,flash,silverlight,html4',

browse_button : 'pickfiles', // you can pass in id...
container: document.getElementById('container'), // ... or DOM Element itself

url : '/photos',
multipart : true,
multipart_params : {
'#{request_forgery_protection_token}': '#{form_authenticity_token}',
'#{Rails.application.config.session_options[:key]}': '#{request.session_options[:id]}',
hotel_id: '#{@hotel.id}'
},

filters : {
max_file_size : '10mb',
mime_types: [
{title : "Image files", extensions : "jpg,gif,png"},
{title : "Zip files", extensions : "zip"}
]
},
resize: {
width: 900,
height: 600
},
      // Flash settings
      flash_swf_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/flash/Moxie.cdn.swf',

      // Silverlight settings
      silverlight_xap_url : 'http://rawgithub.com/moxiecode/moxie/master/bin/silverlight/Moxie.cdn.xap',


      init: {
      PostInit: function() {
      document.getElementById('filelist').innerHTML = '';

      document.getElementById('uploadfiles').onclick = function() {
      uploader.start();
      return false;
      };
      },

      FilesAdded: function(up, files) {
      plupload.each(files, function(file) {
      document.getElementById('filelist').innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
      });
      },

      UploadProgress: function(up, file) {
      document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
      },

      Error: function(up, err) {
      document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;

      }
      }
      });

      uploader.bind('UploadComplete', function() {
      if (uploader.files.length == (uploader.total.uploaded + uploader.total.failed)) {
      $.ajax({
      url: "",
      context: document.body,
      success: function(s,x){
      $(this).html(s);
      }
      });
      $("#console").html('Файлы успешно загружены');
      }
      });

      uploader.bind('FilesAdded', function(up, files) {
      if(up.files.length > 10 || uploader.files.length > 10 || files.length > 10)
      {
      $("#console").html('Можно загружать не более 10 файлов за один раз');
      return false;
      }
      });

      uploader.init();