Rails.application.routes.draw do

  resources :about_pages

  resources :pages

  resources :friendships

  devise_for :users, controllers: {registrations: 'registrations'}

  resources :resorts

  resources :reviews

  resources :room_photos

  resources :rooms

  resources :photos

  resources :services

  resources :hotels

  resources :users

  resources :messages

  resources :events

  get 'profile' => 'users#profile', :as => 'user_profile'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'pages#index'
  get 'payment' => 'pages#payment', as: 'payment'
  get 'not_found' => 'pages#not_found', as: 'not_found'

  namespace :manage do
    resources :manage_hotels
    get 'manage_hotels/:id/upload' => "manage_hotels#upload", :as => 'manage_hotels_upload'
    get 'manage_hotels/:id/rooms' => "manage_hotels#rooms", :as => 'manage_hotels_rooms'

    resources :manage_services
    resources :manage_resorts
    resources :manage_users
    resources :manage_about_pages
    resources :manage_pages
  end

  resources :hotels do
    resources :rooms
    resources :reviews
  end



  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
