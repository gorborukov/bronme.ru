require 'test_helper'

class HotelsControllerTest < ActionController::TestCase
  setup do
    @hotel = hotels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hotels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hotel" do
    assert_difference('Hotel.count') do
      post :create, hotel: { additional: @hotel.additional, address: @hotel.address, checkin: @hotel.checkin, checkout: @hotel.checkout, description: @hotel.description, minimal_price: @hotel.minimal_price, name: @hotel.name, phone: @hotel.phone, price_url: @hotel.price_url, resort_id: @hotel.resort_id, short_description: @hotel.short_description }
    end

    assert_redirected_to hotel_path(assigns(:hotel))
  end

  test "should show hotel" do
    get :show, id: @hotel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hotel
    assert_response :success
  end

  test "should update hotel" do
    patch :update, id: @hotel, hotel: { additional: @hotel.additional, address: @hotel.address, checkin: @hotel.checkin, checkout: @hotel.checkout, description: @hotel.description, minimal_price: @hotel.minimal_price, name: @hotel.name, phone: @hotel.phone, price_url: @hotel.price_url, resort_id: @hotel.resort_id, short_description: @hotel.short_description }
    assert_redirected_to hotel_path(assigns(:hotel))
  end

  test "should destroy hotel" do
    assert_difference('Hotel.count', -1) do
      delete :destroy, id: @hotel
    end

    assert_redirected_to hotels_path
  end
end
