class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :name
      t.string :email
      t.integer :user_id
      t.integer :hotel_id
      t.string :phone
      t.string :tour
      t.text :review

      t.timestamps
    end
  end
end
