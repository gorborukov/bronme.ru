class AddAttachmentImageToRoomPhotos < ActiveRecord::Migration
  def self.up
    change_table :room_photos do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :room_photos, :image
  end
end
