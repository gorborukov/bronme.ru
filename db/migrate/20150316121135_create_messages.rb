class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :sender_id
      t.integer :reciever_id
      t.integer :deadline
      t.integer :claim
      t.text :text
      t.boolean :read

      t.timestamps
    end
  end
end
