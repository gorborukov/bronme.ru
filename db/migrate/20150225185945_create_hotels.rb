class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string :name
      t.text :description
      t.string :short_description
      t.text :additional
      t.string :checkin
      t.string :checkout
      t.integer :minimal_price
      t.string :price_url
      t.string :address
      t.string :phone
      t.integer :resort_id

      t.timestamps
    end
  end
end
