class CreateRoomPhotos < ActiveRecord::Migration
  def change
    create_table :room_photos do |t|
      t.string :name
      t.integer :room_id

      t.timestamps
    end
  end
end
