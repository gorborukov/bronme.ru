class CreateResorts < ActiveRecord::Migration
  def change
    create_table :resorts do |t|
      t.string :name
      t.string :description
      t.string :short_description
      t.boolean :popular

      t.timestamps
    end
  end
end
