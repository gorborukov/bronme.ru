class AddLongitudeAndLatitudeToHotel < ActiveRecord::Migration
  def change
    add_column :hotels, :longitude, :float
    add_column :hotels, :latitude, :float
  end
end
