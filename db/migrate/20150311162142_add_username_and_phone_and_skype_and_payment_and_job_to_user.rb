class AddUsernameAndPhoneAndSkypeAndPaymentAndJobToUser < ActiveRecord::Migration
  def change
    add_column :users, :username, :string
    add_column :users, :phone, :string
    add_column :users, :skype, :string
    add_column :users, :payment, :string
    add_column :users, :job, :string
  end
end
