class AddRoomIdToPhoto < ActiveRecord::Migration
  def change
    add_column :photos, :room_id, :integer
  end
end
