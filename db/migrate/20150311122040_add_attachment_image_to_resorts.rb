class AddAttachmentImageToResorts < ActiveRecord::Migration
  def self.up
    change_table :resorts do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :resorts, :image
  end
end
