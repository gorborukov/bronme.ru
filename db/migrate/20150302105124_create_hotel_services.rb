class CreateHotelServices < ActiveRecord::Migration
  def change
    create_table :hotel_services do |t|
      t.integer :hotel_id
      t.integer :service_id

      t.timestamps
    end
  end
end
