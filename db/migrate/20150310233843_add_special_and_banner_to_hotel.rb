class AddSpecialAndBannerToHotel < ActiveRecord::Migration
  def change
    add_column :hotels, :special, :boolean
    add_column :hotels, :banner, :boolean
  end
end
